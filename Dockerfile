FROM ubuntu:20.04

# Update packages and install necessary dependencies
RUN apt-get update && apt-get install -y gcc sudo

# Set working directory
WORKDIR /app

# Copy the source code files
COPY client/client.c /app/client/
COPY database/database.c /app/database/

# Compile the program
RUN gcc -pthread /app/database/database.c -o /app/database/database
RUN gcc -pthread /app/client/client.c -o /app/client/client

# Set the command to run when the container starts
CMD ["/bin/bash", "-c", "./database/database & sleep 5 && ./client/client"]
