#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <pthread.h>
#include <arpa/inet.h>

#define PORT 8050
#define BUFFER_SIZE 1024

static const char *ERROR = "error";
static const char *ROOT = "root";

int sock = 0, valread;
pthread_t thread;

bool rootCek() {
    return getuid() == 0;
}

void *connectionMsg(void *arg) {
    char receive[256];
    memset(receive, 0, sizeof(receive));
    while (1) {
        read(*(int *)arg, receive, sizeof(receive));
        if (strcmp(receive, "") != 0)
            printf("%s", receive);
        memset(receive, 0, sizeof(receive));
    }
}

bool ClientAuth(int argc, const char *argv[]) {
    char buffer[BUFFER_SIZE];
    memset(buffer, 0, sizeof(buffer));

    if (rootCek()) {
        send(sock, ROOT, strlen(ROOT), 0);
    } else if (argc != 5 || strcmp(argv[1], "-u") != 0 || strcmp(argv[3], "-p") != 0) {
        send(sock, ERROR, strlen(ERROR), 0);
        printf("Authentication failed\n");
        return false;
    } else {
        send(sock, argv[2], strlen(argv[2]), 0);
        valread = read(sock, buffer, BUFFER_SIZE);
        send(sock, argv[4], strlen(argv[4]), 0);
    }
    
    memset(buffer, 0, sizeof(buffer));
    return true;
}


int main(int argc, char const *argv[]) {
    struct sockaddr_in serv_addr;
    char buffer[BUFFER_SIZE];

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
        printf("\nInvalid address\n");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed\n");
        return -1;
    }

    // Connection Success
    if (!ClientAuth(argc, argv))
        return 0;

    memset(buffer, 0, sizeof(buffer));

    printf("'END' to exit\n");

    pthread_t messageThread;
    pthread_create(&messageThread, NULL, &connectionMsg, &sock);

    bool redir = !isatty(fileno(stdin));

    size_t size;
    char *line = NULL;
    if (redir) {
        while (getline(&line, &size, stdin) != -1) {
            send(sock, line, strlen(line), 0);
            memset(line, 0, sizeof(line));
        }
        printf("\n");
        send(sock, "END", strlen("END"), 0);
        valread = read(sock, buffer, sizeof(buffer));
        memset(buffer, 0, sizeof(buffer));
        return 0;
    }

    while (1) {
        scanf("%[^\n]s", buffer);
        getchar();
        send(sock, buffer, strlen(buffer), 0);
        if (strcmp(buffer, "END") == 0) {
            valread = read(sock, buffer, sizeof(buffer));
            return 0;
        }
        memset(buffer, 0, sizeof(buffer));
    }
    return 0;
}
