# sisop-praktikum-fp-2023-HS-E06


## Final Praktikum Sistem Operasi

- 5025211049	Zakia Kolbi
- 5025211210	Nadiah Nuri Aisyah
- 5025211216	Akmal Nafis

# Sistem Database Sederhana

- Program server berjalan sebagai daemon.
- Untuk bisa akses console database, perlu buka program client (kalau di Linux seperti command mysql di bash).
- Program client dan utama berinteraksi lewat socket.
- Program client bisa mengakses server dari mana saja.




# Client

```c
void *connectionMsg(void *arg) {
    char receive[256];
    memset(receive, 0, sizeof(receive));
    while (1) {
        read(*(int *)arg, receive, sizeof(receive));
        if (strcmp(receive, "") != 0)
            printf("%s", receive);
        memset(receive, 0, sizeof(receive));
    }
}

```

Fungsi `connectionMsg` yang Anda berikan merupakan sebuah fungsi dalam bahasa pemrograman C yang menerima sebuah argumen `void *arg`. Fungsi ini menggunakan argumen tersebut sebagai sebuah file descriptor yang mengidentifikasi koneksi atau saluran komunikasi. Fungsi ini secara terus-menerus membaca pesan dari koneksi yang ditentukan oleh file descriptor `arg` dan mencetak pesan tersebut ke layar jika pesan tersebut bukan string kosong.

```c
bool ClientAuth(int argc, const char *argv[]) {
    char buffer[BUFFER_SIZE];
    memset(buffer, 0, sizeof(buffer));

    if (rootCek()) {
        send(sock, ROOT, strlen(ROOT), 0);
    } else if (argc != 5 || strcmp(argv[1], "-u") != 0 || strcmp(argv[3], "-p") != 0) {
        send(sock, ERROR, strlen(ERROR), 0);
        printf("Authentication failed\n");
        return false;
    } else {
        send(sock, argv[2], strlen(argv[2]), 0);
        valread = read(sock, buffer, BUFFER_SIZE);
        send(sock, argv[4], strlen(argv[4]), 0);
    }
    
    memset(buffer, 0, sizeof(buffer));
    return true;
}
```

Fungsi ClientAuth yang Anda berikan merupakan sebuah fungsi dalam bahasa pemrograman C yang mengimplementasikan proses otentikasi pada sisi klien. Fungsi ini menerima argumen argc dan argv[] yang merupakan argumen baris perintah yang diteruskan ke program klien. Fungsi ClientAuth ini bertanggung jawab untuk melakukan otentikasi pada sisi klien dengan mengirimkan informasi pengguna ke server dan memeriksa respons dari server untuk menentukan keberhasilan otentikasi.

```c
 if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
        printf("\nInvalid address\n");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed\n");
        return -1;
    }

    // Connection Success
    if (!ClientAuth(argc, argv))
        return 0;
     memset(buffer, 0, sizeof(buffer));

```

- `if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) { ... }`: Pada langkah ini, sebuah soket (socket) dibuat dengan menggunakan fungsi `socket` dari library sistem. 
- `memset(&serv_addr, '0', sizeof(serv_addr));`: Fungsi `memset` digunakan untuk menginisialisasi semua byte dari variabel `serv_addr` dengan nilai karakter '0'. 
-  `serv_addr.sin_family = AF_INET;`: Setelah inisialisasi, kita mengatur atribut `sin_family` dari `serv_addr` dengan nilai - `AF_INET`, yang menunjukkan bahwa kita akan menggunakan alamat IPv4.
- `serv_addr.sin_port = htons(PORT);`: Atribut `sin_port` dari `serv_addr` diatur dengan nomor port yang akan digunakan untuk koneksi. Fungsi `htons` digunakan untuk mengonversi nomor port ke urutan byte yang sesuai untuk penggunaan jaringan.
- `if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) { ... }`: Fungsi `inet_pton` digunakan untuk mengonversi alamat IPv4 dalam format string ke dalam bentuk yang bisa dipahami oleh struktur data `sin_addr` dari `serv_addr`.
- `if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) { ... }`: Pada langkah ini, program melakukan koneksi ke server dengan menggunakan soket yang telah dibuat sebelumnya. Fungsi `connect` digunakan untuk melakukan koneksi TCP ke alamat server yang ditentukan oleh `serv_addr`. 
- `if (!ClientAuth(argc, argv)) return 0;`: Setelah koneksi berhasil dibuat, program memanggil fungsi `ClientAuth` untuk melakukan proses otentikasi dengan argumen yang diteruskan dari baris perintah.


# Database
```c
pthread_t tid[3000];

typedef struct user_t {
    char name[SIZE];
    char pass[SIZE];
    bool isRoot;
} User;
```
mendefinisikan sebuah tipe data baru bernama User yang memiliki tiga anggota: name, pass, dan isRoot. Anggota-anggota tersebut digunakan untuk menyimpan informasi tentang pengguna, seperti nama pengguna, kata sandi, dan status apakah pengguna tersebut memiliki hak akses sebagai root (administrator).

```c
void makeUser(User* user, char *name, char *pass) {
    strcpy(user->name, name);
    strcpy(user->pass, pass);
}

void printUser(User* user) {
    printf("name: %s\npass: %s\n", user->name, user->pass);
}

bool equalUser(User *a, User *b) {
    return (strcmp(a->name, b->name) == 0 && strcmp(a->pass, b->pass) == 0);
}

bool isAlphanum(char c) {
    if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || (c >= '0' && c <= '9') || c == '*' || c == '=') {
        return true;
    }
    return false;
}
```

- `void makeUser(User* user, char *name, char *pass)`
   Fungsi ini mengisi data pengguna dengan nilai yang diberikan. Menggunakan fungsi `strcpy()`, nama dan kata sandi yang diberikan disalin ke dalam anggota-anggota yang sesuai dari variabel `user`.
- `void printUser(User* user)`
   Fungsi ini mencetak informasi pengguna seperti nama dan kata sandi dengan menggunakan `printf()`. Nilai-nilai tersebut diambil dari variabel `user`.
- `bool equalUser(User *a, User *b)`
   Fungsi ini membandingkan dua objek pengguna (`User`) berdasarkan nama dan kata sandi mereka. Menggunakan fungsi `strcmp()`, fungsi ini mengembalikan nilai `true` jika nama dan kata sandi keduanya sama, dan `false` jika tidak sama.
- `bool isAlphanum(char c)`
   Fungsi ini memeriksa apakah karakter yang diberikan (`c`) adalah karakter alfanumerik atau tidak. Karakter dianggap alfanumerik jika merupakan huruf (baik huruf besar maupun kecil), angka, atau karakter khusus seperti '*' atau '='. 


Beberapa fungsi yang ada dalam file database.c

- `void splitCommands(const char *source, char dest[MAX_COMMANDS][MAX_COMMAND_LENGTH], int *command_size)`
   Fungsi ini digunakan untuk memecah string perintah menjadi token-token terpisah. Setiap token akan disimpan dalam array `dest`, sedangkan `command_size` akan menyimpan jumlah token yang berhasil dipisahkan. Fungsi ini menggunakan fungsi `isAlphanum()` untuk memeriksa apakah karakter non-alphanumerik.

```c
void createDatabase(char *name) {
    mkdir(DB_PROG_NAME, 0777);
    char buff[256];

    sprintf(buff, "%s/%s", DB_PROG_NAME, name);
    mkdir(buff, 0777);
}
```
-  `void createDatabase(char *name)`
   Fungsi ini membuat direktori dengan nama `name` menggunakan fungsi `mkdir()`. Direktori tersebut akan dibuat dengan izin 0777 (hak akses penuh).

```c
void createTable(char *db, char *tb, char *attr[64], int size, char dt[MAX_COLUMN][32], int dt_size) {
    char buff[256];
    sprintf(buff, "%s/%s/%s.info", DB_PROG_NAME, db, tb);
    FILE *fptr = fopen(buff, "w");
    if (!fptr) return;
    for (int i = 0; i < size; i++) {
        fprintf(fptr, "%s,", attr[i]);
    }
    fclose(fptr);

    sprintf(buff, "%s/%s/.%s_nya", DB_PROG_NAME, db, tb);
    fptr = fopen(buff, "w");
    if (fptr) {
        fprintf(fptr, "CREATE TABLE %s (", tb);
        for (int i = 0; i < dt_size; i++) {
            fprintf(fptr, "%s %s", attr[i], dt[i]);
            if (i != dt_size-1) fprintf(fptr, ", ");
        }
        fprintf(fptr, ");");
        fclose(fptr);
    }
}
```
- `void createTable(char *db, char *tb, char *attr[64], int size, char dt[MAX_COLUMN][32], int dt_size)`
   Fungsi ini digunakan untuk membuat sebuah tabel dalam database. Fungsi ini akan membuat file `.info` yang berisi atribut-atribut tabel, serta file `.nya` yang berisi perintah SQL untuk membuat tabel. Atribut-atribut tabel dan tipe data akan diberikan melalui argumen fungsi.

```c
void insertToTable(char *db, char *tb, char *attr_data[64], int size) {
    char buff[256];
    sprintf(buff, "%s/%s/%s.info", DB_PROG_NAME, db, tb);
    FILE *fptr = fopen(buff, "a");
    if (!fptr) return;
    fprintf(fptr, "\n");
    for (int i = 0; i < size; i++) {
        fprintf(fptr, "%s,", attr_data[i]);
    }
    fclose(fptr);
    printf("[Log] INSERT INTO %s.%s (", db, tb);
    for (int i = 0; i < size; i++) {
        printf("%s", attr_data[i]);
        if (i != size-1) printf(", ");
    }
    printf(")\n");
}
```
- `void insertToTable(char *db, char *tb, char *attr_data[64], int size)`
   Fungsi ini digunakan untuk memasukkan data ke dalam tabel. Data akan disimpan dalam file `.info` dari tabel yang ditentukan dalam database yang ditentukan. Fungsi ini juga akan mencetak perintah SQL INSERT INTO yang sesuai dengan data yang dimasukkan.

- `bool doesDatabaseExist(const char name[])`
   Fungsi ini digunakan untuk memeriksa apakah database dengan nama yang diberikan sudah ada. Fungsi ini mengembalikan `true` jika database sudah ada, dan `false` jika tidak ada.
-  `bool doesTableExist(char *db, char *tb)`
   Fungsi ini digunakan untuk memeriksa apakah tabel dengan nama yang diberikan sudah ada dalam database yang ditentukan. Fungsi ini mengembalikan `true` jika tabel sudah ada, dan `false` jika tidak ada.
- `void createUser(char *u, char *p)`
   Fungsi ini digunakan untuk membuat pengguna baru dengan nama pengguna (`u`) dan kata sandi (`p`) yang diberikan. Fungsi ini memasukkan data pengguna ke dalam tabel pengguna menggunakan fungsi `insertToTable()`.
- `void __createUsersTable()`
   Fungsi ini digunakan untuk membuat tabel pengguna jika tabel tersebut belum ada. Fungsi ini menggunakan fungsi `doesTableExist()` untuk memeriksa keberadaan tabel dan `createTable()` untuk membuat tabel baru jika diperlukan.
- `void __createPermissionsTable()`
   Fungsi ini digunakan untuk membuat tabel izin jika tabel tersebut belum ada. Fungsi ini menggunakan fungsi `doesTableExist()` untuk memeriksa keberadaan tabel dan `createTable()` untuk membuat tabel baru jika diperlukan.
- `void AuthInit()`
   Fungsi ini digunakan untuk menginisialisasi sistem otentikasi. Fungsi ini memeriksa keberadaan database otentikasi dan membuat tabel pengguna dan tabel izin jika belum ada.
- `void __readUserTable(User *user, char *line)`
   Fungsi ini digunakan untuk membaca baris data pengguna dari file tabel pengguna. Fungsi ini memisahkan nama pengguna dan kata sandi dari baris yang diberikan dan menyimpannya dalam struktur `User`.
- `bool NormalUserAuth(User *user)`
   Fungsi ini digunakan untuk melakukan otentikasi pengguna biasa (non-root). Fungsi ini membaca data pengguna dari file tabel pengguna dan membandingkannya dengan pengguna yang diberikan. Fungsi ini mengembalikan `true` jika otentikasi berhasil, dan `false` jika tidak berhasil.
- `bool ClientAuth(User* user)`
   Fungsi ini digunakan untuk melakukan otentikasi pengguna yang diberikan. Fungsi ini memeriksa apakah pengguna adalah pengguna root atau pengguna biasa. Fungsi ini menggunakan fungsi `NormalUserAuth()` untuk otentikasi pengguna biasa. Fungsi ini mengembalikan `true` jika otentikasi berhasil, dan `false` jika tidak berhasil.
- `void MessageSentByDB(int *new_socket, char *msg)`
    Fungsi ini digunakan untuk mengirim pesan melalui soket. Fungsi ini menggunakan `send()` untuk mengirim pesan dengan panjang yang ditentukan.

- Fungsi UpdatedInsideTable() digunakan untuk memperbarui nilai kolom tertentu dalam tabel yang ada di dalam database
- Fungsi `ColumnDrop()`: Fungsi ini digunakan untuk menghapus kolom dari sebuah tabel dalam database. Fungsi ini membaca file tabel yang ada, mencari kolom yang akan dihapus, dan menulis kembali isi tabel tanpa mencantumkan kolom yang dihapus. Fungsi ini mengembalikan jumlah baris yang terpengaruh oleh penghapusan kolom.
- Fungsi `RemoveFromTable()`: Fungsi ini digunakan untuk menghapus baris yang memenuhi kondisi tertentu dari sebuah tabel dalam database. Fungsi ini membaca file tabel, mencari baris yang akan dihapus berdasarkan kondisi yang diberikan, dan menulis kembali isi tabel tanpa mencantumkan baris yang dihapus. Fungsi ini mengembalikan jumlah baris yang terpengaruh oleh penghapusan.
- Fungsi `grantPermission()`: Fungsi ini digunakan untuk memberikan izin akses ke database kepada seorang pengguna. Fungsi ini memasukkan data izin ke dalam tabel yang mengatur izin akses.
- Fungsi `hasPermissionToDB()`: Fungsi ini digunakan untuk memeriksa apakah seorang pengguna memiliki izin akses ke suatu database. Fungsi ini membaca tabel izin akses dan memeriksa apakah ada entri yang sesuai dengan nama pengguna dan nama database yang diberikan.
- Fungsi `dropDatabase()`: Fungsi ini digunakan untuk menghapus sebuah database beserta seluruh isinya. Fungsi ini menghapus direktori database beserta semua file dan direktori di dalamnya.
- Fungsi `dropTable()`: Fungsi ini digunakan untuk menghapus sebuah tabel dari database. Fungsi ini menghapus file tabel dari direktori database.

```c
void logging(User *user, char *command) {
    time_t t = time(NULL);
    struct tm* lt = localtime(&t);

    char currTime[TIME_SIZE];
    strftime(currTime, TIME_SIZE, "%Y-%m-%d %H:%M:%S", lt);

    char log[LOG_SIZE];
    sprintf(log, "%s:%s:%s", currTime, user->name, command);

    FILE *out = fopen(logpath, "a");
    fprintf(out, "%s\n", log);
    fclose(out);
}
```
- Fungsi `logging()`: Fungsi ini digunakan untuk mencatat aktivitas pengguna dalam file log. Fungsi ini mencatat waktu, nama pengguna, dan perintah yang dilakukan oleh pengguna ke dalam file log.
Fungsi-fungsi ini membantu dalam mengelola dan memanipulasi database, termasuk operasi seperti menghapus kolom atau tabel, memberikan izin akses, dan mencatat log aktivitas pengguna.

Fungsi `client` di atas adalah fungsi yang dijalankan oleh thread untuk melayani koneksi dari klien. Fungsi ini menerima argumen berupa socket klien yang baru, membaca input dari klien, dan mengeksekusi perintah yang diberikan oleh klien sesuai dengan protokol yang ditentukan. Fungsi ini juga melakukan autentikasi pengguna dan memproses perintah yang diterima, seperti membuat basis data, membuat tabel, menyisipkan data, mengambil data, menghapus data, dll. Setelah selesai memproses perintah, koneksi dengan klien ditutup.

- Fungsi menerima argumen `void *tmp`, yang kemungkinan berisi socket descriptor untuk koneksi client yang baru.
- Sebuah array karakter `buffer` dengan ukuran 1024 dideklarasikan. Fungsi `memset` digunakan untuk menginisialisasi semua elemen array `buffer` dengan nilai 0. Hal ini dilakukan untuk memastikan bahwa array tersebut kosong sebelum digunakan.
- Dua variabel lokal dideklarasikan: `valread` dan `new_socket`. Variabel `new_socket` diinisialisasi dengan nilai dari pointer `tmp` yang dideferensiasi terlebih dahulu dengan cast `(int *)tmp`. Ini mengasumsikan bahwa argumen `tmp` adalah alamat memori dari sebuah integer yang menyimpan socket descriptor.
- Sebuah objek `User` dengan nama `current` dideklarasikan. Ini mungkin merupakan struktur atau kelas yang digunakan untuk menyimpan informasi tentang pengguna saat ini.
- Fungsi `read` dipanggil untuk membaca data yang diterima dari socket yang terhubung dengan client. Data yang dibaca disimpan dalam array `buffer` dengan ukuran 1024. Jumlah byte yang berhasil dibaca disimpan dalam variabel `valread`.

```c
int main(int argc, char const *argv[]) {
  pid_t pid, sid;      

  pid = fork();     
  if (pid < 0) {
    exit(EXIT_FAILURE);
  }

  if (pid > 0) {
    exit(EXIT_SUCCESS);
  }

  umask(0);

  sid = setsid();
  if (sid < 0) {
    exit(EXIT_FAILURE);
  }

  close(STDOUT_FILENO);
  close(STDERR_FILENO);
    
  while(1){
        int new_socket;
      
        if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
            perror("socket failed");
            exit(EXIT_FAILURE);
        }
        if (setsockopt(server_fd, SOL_SOCKET, 
            SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
            perror("setsockopt");
            exit(EXIT_FAILURE);
        }

        address.sin_family = AF_INET;
        address.sin_addr.s_addr = INADDR_ANY;
        address.sin_port = htons(PORT);

        if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
            perror("bind failed");
            exit(EXIT_FAILURE);
        }

        if (listen(server_fd, 5) < 0) {
            perror("listen");
            exit(EXIT_FAILURE);
        }
        AuthInit();

        while(true) {
            if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
            perror("accept");
            exit(EXIT_FAILURE);
        }
        pthread_create(&(tid[ctrClient]), NULL, &client, &new_socket);
        ctrClient++;
    }
  }
    return 0;
} 
```

- `pid_t pid, sid;` mendeklarasikan dua variabel bertipe `pid_t` yang digunakan untuk menyimpan identifier proses dan session.
- `pid = fork();` melakukan pemanggilan sistem `fork()`, yang menghasilkan duplikat dari proses saat ini. Nilai yang dikembalikan (`pid`) akan berbeda tergantung apakah proses yang berjalan saat ini adalah proses induk (nilai `pid` adalah identifier dari proses anak) atau proses anak (nilai `pid` adalah 0).
-  Pada bagian ini dilakukan pemeriksaan nilai `pid` untuk memastikan apakah fork berhasil dilakukan atau tidak. Jika `pid` kurang dari 0, artinya fork gagal, maka program akan keluar dengan kode kesalahan (`EXIT_FAILURE`).
- Pada bagian ini dilakukan pengecekan jika nilai `pid` lebih besar dari 0, artinya proses saat ini adalah proses induk. Dalam hal ini, proses induk akan keluar dengan kode sukses (`EXIT_SUCCESS`). Proses anak akan melanjutkan eksekusi kode di bawahnya.
- `umask(0);` memanggil fungsi `umask` untuk mengatur mode izin (file permission) yang akan digunakan oleh proses anak. Nilai 0 mengindikasikan bahwa tidak ada perubahan pada mode izin yang akan dilakukan.
- `sid = setsid();` memanggil fungsi `setsid` untuk membuat sesi baru untuk proses anak. Fungsi ini mengembalikan identifier sesi baru yang disimpan dalam variabel `sid`. Jika nilai `sid` kurang dari 0, artinya pembuatan sesi gagal, maka program akan keluar dengan kode kesalahan.
- `close(STDOUT_FILENO);` dan `close(STDERR_FILENO);` menutup file descriptor standar untuk output (STDOUT) dan error (STDERR). Dengan demikian, proses anak tidak akan mengeluarkan output ke layar dan error akan diabaikan.
- Pada bagian ini terdapat perulangan `while(1)` yang akan berjalan terus menerus. Di dalam perulangan ini, proses anak akan melakukan beberapa operasi.
- Pertama, dilakukan inisialisasi socket menggunakan fungsi `socket()`. Jika pemanggilan sistem ini mengembalikan nilai 0, artinya gagal membuat socket, maka program akan keluar dengan kode kesalahan.
-  Selanjutnya, dilakukan penyetelan opsi socket menggunakan fungsi `setsockopt()` untuk memungkinkan penggunaan alamat (address) dan port yang sama setelah aplikasi selesai dieksekusi. Jika pemanggilan sistem ini mengembalikan nilai selain 0, artinya terjadi kesalahan pada penyetelan opsi socket, maka program akan keluar dengan kode kesalahan.

- Variabel `address` diisi dengan alamat IP loopback (`INADDR_ANY`) dan nomor port yang ditentukan (`PORT`).
-  Fungsi `bind()` digunakan untuk mengikat (bind) socket dengan alamat dan port yang telah ditentukan. Jika pemanggilan sistem ini mengembalikan nilai kurang dari 0, artinya terjadi kesalahan dalam mengikat socket, dan program akan keluar dengan kode kesalahan.
- Setelah socket berhasil diikat, dilakukan pemanggilan fungsi `listen()` untuk memulai mendengarkan koneksi masuk (incoming connection) pada socket. Jika pemanggilan sistem ini mengembalikan nilai kurang dari 0, artinya terjadi kesalahan dalam mendengarkan koneksi, dan program akan keluar dengan kode kesalahan.
- Fungsi `AuthInit()` dipanggil untuk melakukan inisialisasi proses autentikasi.
-  Selanjutnya, terdapat perulangan `while(true)` yang akan berjalan terus menerus untuk menerima koneksi masuk dari client.
- Di dalam perulangan, pemanggilan fungsi `accept()` digunakan untuk menerima koneksi masuk dari client. Jika pemanggilan sistem ini mengembalikan nilai kurang dari 0, artinya terjadi kesalahan dalam menerima koneksi, dan program akan keluar dengan kode kesalahan.
- Setelah berhasil menerima koneksi, dilakukan pemanggilan fungsi `pthread_create()` untuk membuat thread baru yang akan menangani koneksi dengan client. Parameter `&(tid[ctrClient])` merupakan alamat dari variabel thread identifier yang akan disimpan, `NULL` merupakan pointer ke atribut thread yang akan diabaikan, `&client` adalah alamat dari variabel `client` yang berisi socket yang terhubung dengan client, dan `&new_socket` adalah alamat dari variabel `new_socket` yang digunakan dalam thread untuk mengakses socket tersebut.
- Variabel `ctrClient` digunakan untuk menghitung jumlah thread yang telah dibuat.
- Setelah membuat thread, penanda `ctrClient` akan diincrement untuk menandakan bahwa thread baru telah dibuat.
- Program akan tetap berjalan dalam perulangan ini untuk terus menerima koneksi masuk dan membuat thread untuk menghandle koneksi tersebut.
- Jika terdapat kondisi atau logika lain dalam perulangan yang menentukan kapan perulangan tersebut akan berhenti, maka perlu ditambahkan dalam kode tersebut.
- Akhirnya, program akan mengembalikan nilai 0 untuk menandakan bahwa program telah selesai dijalankan tanpa terjadi kesalahan.


# Containerization

- Dockerfile
```c
FROM ubuntu:20.04

# Update packages and install necessary dependencies
RUN apt-get update && apt-get install -y gcc sudo

# Set working directory
WORKDIR /app

# Copy the source code files
COPY client/client.c /app/client/
COPY database/database.c /app/database/

# Compile the program
RUN gcc -pthread /app/database/database.c -o /app/database/database
RUN gcc -pthread /app/client/client.c -o /app/client/client

# Set the command to run when the container starts
CMD ["/bin/bash", "-c", "./database/database & sleep 5 && ./client/client"]
```
- Menggunakan base image ubuntu:20.04.
- Melakukan update pada paket-paket yang ada di dalam image dan menginstal dependensi yang diperlukan, yaitu gcc dan sudo.
- Mengatur working directory di dalam container menjadi /app.
- Menyalin file source code client.c ke dalam direktori /app/client di dalam container.
- Menyalin file source code database.c ke dalam direktori /app/database di dalam container.
- Mengompilasi program database.c menggunakan gcc dengan opsi -pthread dan menyimpan hasil kompilasi ke dalam file /app/database/database.
- Mengompilasi program client.c menggunakan gcc dengan opsi -pthread dan menyimpan hasil kompilasi ke dalam file /app/client/client.
- Mengatur perintah yang akan dijalankan ketika container dimulai, yaitu menjalankan program database dan program client secara bersamaan dengan menggunakan /bin/bash sebagai shell.

- Jalankan Dockerfile untuk membuat image
![image](https://cdn.discordapp.com/attachments/945203039034306570/1122476770520735754/Screenshot_2023-06-25_163811.png)
![image](https://cdn.discordapp.com/attachments/945203039034306570/1122476770084536381/Screenshot_2023-06-25_163818.png)

- Jalankan Image
![image](https://cdn.discordapp.com/attachments/945203039034306570/1122476770084536381/Screenshot_2023-06-25_163818.png)
- Publish Dockerimage
![image](https://cdn.discordapp.com/attachments/945203039034306570/1122476769795113021/Screenshot_2023-06-25_163827.png)
- menerapkan skala pada layanan menggunakan Docker Compose dengan instance sebanyak 5. Dengan contoh Folder Gebang
![image](https://cdn.discordapp.com/attachments/945203039034306570/1122507465393721434/Screenshot_2023-06-25_194330.png)

# Menjalankan Program

- Jalankan program client dengan `sudo ./client`. kemudian create user. dan jalankan program dengan user dan password. Create Database kemudian pilih database, kemudian buat table 
![image](https://cdn.discordapp.com/attachments/945203039034306570/1122476768838828042/Screenshot_2023-06-25_173705.png)
- Insert pada tabel kemudian query
![image](https://cdn.discordapp.com/attachments/945203039034306570/1122476767278538813/Screenshot_2023-06-25_173851.png)
- Drop Tabel
![image](https://cdn.discordapp.com/attachments/945203039034306570/1122476768473915473/Screenshot_2023-06-25_173734.png)
- Delete row dari tabel 
![image](https://cdn.discordapp.com/attachments/945203039034306570/1122476792763134012/Screenshot_2023-06-25_173904.png)
- logging 
![image](https://cdn.discordapp.com/attachments/945203039034306570/1122476792075255838/Screenshot_2023-06-25_173921.png)

# Kesulitan
1. Membuat stuktur database, menjalankan fungsi yang digunakan. Memproses input dari client. Menghubungkan client dan database dengan socket
2. Mengolah konversi data dari client, tidak familiar dengan program yang harus digunakan








